import random

import pygame
pygame.init()
screen = pygame.display.set_mode((300, 300))
x = 140
y = 140
rect = pygame.Rect(x, y, 10, 10)
color = (113, 86, 118)
color1 = (150, 189, 198)
color2 = (210, 224, 191)
color3 = (73, 81, 219)
color4 = (70, 73, 76)
color5 = (85, 12, 24)
color6 = (249, 220, 92)
color7 = (73, 81, 111)
color_list=[color1, color2, color3, color4, color5, color6, color7]
left = False
right = False
up = False
down = False
color_change = False
while True:
    screen.fill((191, 155, 183))
    pygame.time.delay(30)
    for event in pygame.event.get():
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_c:
                color_change = True
            if event.key == pygame.K_LEFT:
                left = True
            if event.key == pygame.K_RIGHT:
                right = True
            if event.key == pygame.K_DOWN:
                down = True
            if event.key == pygame.K_UP:
                up = True
        if event.type == pygame.KEYUP:
            if event.key == pygame.K_c:
                color_change = False
            if event.key == pygame.K_LEFT:
                left = False
            if event.key == pygame.K_RIGHT:
                right = False
            if event.key == pygame.K_DOWN:
                down = False
            if event.key == pygame.K_UP:
                up = False
        if event.type == pygame.QUIT:
            pygame.quit()
            exit()
    if color_change:
        color = random.choice(color_list)
        color_change = False
    if right and x != 290:
        x += 10
        rect = pygame.Rect(x, y, 10, 10)
    if left and x != 0:
        x -= 10
        rect = pygame.Rect(x, y, 10, 10)
    if down and y != 290:
        y += 10
        rect = pygame.Rect(x, y, 10, 10)
    if up and y != 0:
        y -= 10
        rect = pygame.Rect(x, y, 10, 10)
    pygame.draw.rect(screen, color, rect, 0)
    pygame.display.update()
