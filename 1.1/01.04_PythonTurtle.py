import turtle
from turtle import *
def flower(i=1):
     for x in range(100):
          circle.pencolor(color[x%6])
          circle.circle(x*i)
          circle.right(60)

turtle.bgcolor("#EFE7ED")
circle=turtle.Pen()
circle.ht()
color=["#A06A91","#798686","#88D18A","#00A6ED","#218380","#D0888B"]
window=turtle.Screen()
window.tracer(0)
turtle.ht()
turtle.speed(10)
turtle.delay(0)
i=0.5
a=0.04
for j in range(400):
     circle.goto(2*j, 2*j)
     circle.clear()
     flower(i)
     window.update()
     circle.home()
     circle.right(j)
     i+=a
     if(i>3 or i<0.1):
          a-=a
